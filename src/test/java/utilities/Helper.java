package utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Helper {

	// Method to take screenshot when Test cases Fail
	public static void captureFAIL_Screenshot(WebDriver driver, String screenshotname) {
		// Save the capture screen shot into the destination created folder with the
		// name of the test case and date
		Path dest = Paths.get("./Fail_Screenshots",
				screenshotname + new SimpleDateFormat(" yyyy-MM-dd-HH-mm").format(new Date()) + ".png");

		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (IOException e) {
			System.out.print("Exception while taking screen shot" + e.getMessage());
		}
	}

	// Method to generate a random password
	public static String getRandomPassword() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	// Method to generate a random Gmail
	public static String getRandomGmail() {

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		// element.sendKeys("username"+ randomInt +"@gmail.com");
		String emailString = "username" + randomInt + "@gmail.com";
		return emailString;
	}

	// Get current time/date for the reports
	public static String getCurrentTime() {
		DateFormat format = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		//DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = new Date();
		return format.format(date);
	}
}
