package tests;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utilities.Helper;
import utilities.PropertyManager;

public class TestBase {
	public static WebDriver driver;
	public static ExtentReports extentreport;
	public static ExtentTest logger;

	// Get data from the property manager class and get the values from the
	// properties file
	String baseURL = PropertyManager.getInstance().getExpectedRegisterURL();
	String browserName = PropertyManager.getInstance().getExpectedBrowserName();
	String OS = PropertyManager.getInstance().getExpectedOS();

	// Locate report file
	String ReportfolderPath = System.getProperty("user.dir") + "/TestReport/PLTaskReport" + Helper.getCurrentTime()
			+ ".html";

	@BeforeSuite
	public void Start() {
		/// Generate extent report within the folder path
		extentreport = new ExtentReports(ReportfolderPath, true);
		extentreport.addSystemInfo("OS: ", OS);
		extentreport.addSystemInfo("Browser: ", browserName);
		extentreport.addSystemInfo("Language: ", "JAVA");

	}

	@AfterSuite
	public void End() {
		extentreport.flush();
	}

	@BeforeClass()
	protected void startDriver() {

		if (OS.equalsIgnoreCase("windows")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver.exe");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}

		} else if (OS.equalsIgnoreCase("mac")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver_mac");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver_mac");
				driver = new FirefoxDriver();
			}

		} else if (OS.equalsIgnoreCase("linux")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver_linux");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver_linux");
				driver = new FirefoxDriver();
			}

		}

		driver.manage().window().maximize();

		driver.navigate().to(baseURL);

	}

	@AfterClass
	protected void exitDriver() {

		driver.quit();

	}

	// Take Screen-shot when test case fails and add it to the screen-shot folder
	// and log extent report status
	@AfterMethod
	protected static void screenshotOnFailure(ITestResult result) throws InterruptedException {
		try {
			if (result.getStatus() == ITestResult.FAILURE) {
				try {
					System.out.println("Taking Screenshot....");
					System.out.println("Failed!");
					Helper.captureFAIL_Screenshot(driver, result.getName().concat(" [Failed]"));
					logger.log(LogStatus.FAIL, "Test Fail");

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Failed to add screenshot in the Report");
				}

			} else if (result.getStatus() == ITestResult.SUCCESS) {
				logger.log(LogStatus.PASS, "Test Pass");
			} else {
				logger.log(LogStatus.SKIP, "Test Skipped");

			}

		} catch (Exception e) {
			System.out.println("Exception in screenShotOn Fail Method !");
		}

	}

	// Get the test-case name before each execution for the report
	@BeforeMethod
	protected static void setLogs(Method method) {
		logger = extentreport.startTest(method.getName());

	}

	protected String getActualPageTitle() {

		String actual = driver.getTitle();
		return actual;
	}

	protected String getActualPageURL() {
		String actual = driver.getCurrentUrl();
		return actual;
	}

}
