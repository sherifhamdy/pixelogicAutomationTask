package tests;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.ahmed.excelizer.ExcelReader;
import pages.LoginPage;
import pages.RegistrationPage;
import utilities.Helper;
import utilities.PropertyManager;

public class RegistrationTestCases extends TestBase {

	// Objects declaration
	RegistrationPage registrationPageObject = null;
	LoginPage loginPageObject = null;

	// Excel sheet path and Pre-Condition
	String testDataPath = "/TestData/registerationData.xlsx";
	String sheetName_1 = "Sheet1";
	String sheetName_2 = "Sheet2";
	String sheetName_3 = "Sheet3";
	String TestDataFilePath = System.getProperty("user.dir") + testDataPath;

	// Get data from the first sheet in the excel file
	@DataProvider(name = "validationTrials")
	public Object[][] validationTrials() {

		return ExcelReader.loadTestData(TestDataFilePath, sheetName_1);

	}

	// Get data from the second sheet in the excel file
	@DataProvider(name = "RandomProperData")
	public Object[][] RandomProperData() {

		return ExcelReader.loadTestData(TestDataFilePath, sheetName_2);

	}

	// Get data from the third sheet in the excel file
	@DataProvider(name = "StaticProperData")
	public Object[][] StaticProperData() {

		return ExcelReader.loadTestData(TestDataFilePath, sheetName_3);

	}

	// Objects Initialization
	@BeforeClass
	public void initPageObject() {
		registrationPageObject = new RegistrationPage(driver);
		loginPageObject = new LoginPage(driver);

	}

	// First test-case to check the redirection of the register URl
	@Test(priority = 1, enabled = true, description = "Check the redirection of the URL")
	public void Sucessfull_URL_Rediredction() throws InterruptedException {

		// Get the expected values from the properties file
		String expectedPageURL = PropertyManager.getInstance().getExpectedRegisterURL();
		String expectedPageTitle = PropertyManager.getInstance().getExpectedRegisterPageTitle();

		// Get the actual values from the web-site
		String ActualPageTitle = getActualPageTitle();
		String ActualPageURL = getActualPageURL();
		// Values Validation
		assertEquals(ActualPageURL, expectedPageURL);
		assertEquals(ActualPageTitle, expectedPageTitle);
		// Print Values
		System.out.println("ActualURL: [ " + ActualPageURL + " ] & Expected: [ " + expectedPageURL + " ]");
		System.out.println("ActualURL: [ " + ActualPageTitle + " ] & Expected: [ " + expectedPageTitle + " ]");
	}

	// Second test-case to check the registration fields are displayed properly
	@Test(priority = 2, enabled = true, dependsOnMethods = "Sucessfull_URL_Rediredction", description = "Check the registration form content is displayed properly")
	public void Proper_Registration_Content() {

		// Validate the fields retrieved true
		boolean actualResult = registrationPageObject.registerContentIsDisplayed();
		Assert.assertTrue(actualResult);
	}

	// Third test-case to check the error messages are displayed properly,
	// compared with the expected values from the excel sheet
	@Test(priority = 3, enabled = true, dependsOnMethods = "Sucessfull_URL_Rediredction", description = "Check the registered user can login successfully ", dataProvider = "validationTrials")
	public void RegistrationValidationTrials(String FN, String LN, String EM, String Pass, String ConfPass, String Mob,
			String ExpectedError, String ExpectedURL) throws InterruptedException {

		// Get the actual values from the web-site
		String actualErrorResult = registrationPageObject.newFullRegister(FN, LN, EM, Pass, ConfPass, Mob);
		String actualURLResult = getActualPageURL();

		// Values Validation
		assertEquals(actualErrorResult, ExpectedError);
		assertEquals(actualURLResult, ExpectedURL);

	}

	// Fourth test-case to login after the registration with a random email and
	// password
	@Test(priority = 4, enabled = true, dependsOnMethods = "Sucessfull_URL_Rediredction", description = "Check the registered user can login successfully ", dataProvider = "RandomProperData")
	public void Random_Successful_Login(String FirstName, String LastName, String MobileNumber)
			throws InterruptedException, AWTException {

		// Get random email, password and confirm password
		String RandomEmail = Helper.getRandomGmail();
		String RandomPassword = Helper.getRandomPassword();
		String ConfirmPassword = RandomPassword;

		// Register with the above data - Valid Register
		registrationPageObject.successfullRegister(FirstName, LastName, RandomEmail, RandomPassword, ConfirmPassword,
				MobileNumber);

		// Validate that the user logged in properly with the above user-name and
		// password
		Assert.assertTrue(loginPageObject.successloginWelcome(FirstName, LastName));

		// Get the expected page title of the home-screen after login,
		// compared with the actual page title from the web-site
		String expectedPageTitle = PropertyManager.getInstance().getExpectedmyAccoutPageTitle();
		String actualPageName = getActualPageTitle();

		// Values Validation
		assertEquals(actualPageName, expectedPageTitle);

		// Logout after being logged in
		loginPageObject.logout();

		// Get actual and expected URL
		String actualURLResult = getActualPageURL();
		String expectedURLResult = PropertyManager.getInstance().getExpectedLoginURL();

		// Values Validation
		assertEquals(actualURLResult, expectedURLResult);

	}

	// Check the registration with pre-defined valid data from the excel sheet which
	// require to delete the registered account from the DB to run the script again.
	// Check the verification email recieved from a one time registered data
	@Test(priority = 5, enabled = false, dependsOnMethods = "Sucessfull_URL_Rediredction", description = "Check the registered user can login successfully ", dataProvider = "StaticProperData")
	public void Static_Successful_Login(String FirstName, String LastName, String Email, String Pass, String ConfPass,
			String Mob) throws InterruptedException, AWTException {

		// Valid one time registration
		registrationPageObject.successfullRegister(FirstName, LastName, Email, Pass, ConfPass, Mob);

		// Assert user logged in properly with the above user-name and password
		Assert.assertTrue(loginPageObject.successloginWelcome(FirstName, LastName));

		// Get and validate the expected and actual page title
		String expectedPageTitle = PropertyManager.getInstance().getExpectedmyAccoutPageTitle();
		String actualPageName = getActualPageTitle();
		assertEquals(actualPageName, expectedPageTitle);

		// Then Login to Gmail, Validate on the verification email

	}

}
