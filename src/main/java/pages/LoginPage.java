package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase {

	// Driver linkage
	public LoginPage(WebDriver driver) {
		super(driver);

	}

	// Detecting Elements from the UI in Login screen

	@FindBy(xpath = "//*[@class='text-align-left']")
	public WebElement welcomeHeader;

	@FindBy(xpath = "(//*[@id='dropdownCurrency'])[2]")
	public WebElement topHeader;

	@FindBy(xpath = "(//*[@id='dropdownCurrency'])[2]/../*[2]/*[1]") //
	public WebElement topHeaderlist;

	@FindBy(xpath = "(//*[@id='dropdownCurrency'])[2]/../*[2]/*[1]/*[2]") //
	public WebElement logoutBTN;

	// Validate Home screen is displayed with proper welcome user-name
	public boolean successloginWelcome(String firstname, String lastname) {
		boolean flag = false;

		try {
			WaitForElement(welcomeHeader);
			String labelString = welcomeHeader.getText();

			if (labelString.contentEquals("Hi, " + firstname + " " + lastname)) {
				flag = true;

			} else {
				flag = false;
			}

		} catch (Exception e) {
			flag = false;
			System.out.println("exception in welcome login: " + e);
		}
		return flag;

	}

	public void logout() {

		try {
			WaitForElement(topHeader);
			clickButton(topHeader);
			WaitForElement(logoutBTN);
			clickButton(logoutBTN);
		} catch (Exception e) {
			System.out.println("exception in welcome login while logout : " + e);
		}

	}
}
