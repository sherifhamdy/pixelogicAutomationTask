package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {

	protected static WebDriver driver2;
	protected static WebDriverWait wait = null;
	protected static Actions actions = null;
	protected static JavascriptExecutor js = null;
	protected Select elementSelection = null;

	// create constructor
	public PageBase(WebDriver driver) {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 30);
		actions = new Actions(driver);
		js = (JavascriptExecutor) driver;
		driver2 = driver;

	}

	// Method to Click Buttons
	protected void clickButton(WebElement button) {
		button.click();
	}

	// Method to write on field
	protected void setText(WebElement textElement, String value) {

		WaitForElement(textElement);
		textElement.clear();
		textElement.sendKeys(value);
	}

	// Method to wait for an element until visibility
	protected void WaitForElement(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));

		} catch (Exception e) {
			System.out.println("ex: " + e);
		}
	}

	// Method to scroll to an element by position
	protected static void scrollByPosition(WebElement element) {

		// get position
		int x = 0;
		int y = element.getLocation().getY();

		// scroll to x y
		js.executeScript("window.scrollBy(" + x + ", " + y + ")");

	}

	// Method to scroll to an element
	protected void scrollByactions(WebElement element) {

		// Get the current browser name to perform it's desired scrolling function
		Capabilities cap = ((RemoteWebDriver) driver2).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();

		if (browserName.contains("chrome")) {
			actions.moveToElement(element).perform();
		} else {
			// in-case of fire-fox scroll with the below function
			js.executeScript("arguments[0].scrollIntoView(true);", element);
		}

	}

	// Method to open new tab
	protected void openNewTab(String url) {
		actions.sendKeys(Keys.CONTROL + "t");
		js.executeScript("window.open('" + url + "','_blank');");

	}

	// Method to select from the top header and logout
	protected void selectElement(WebElement selectWebElement, String option) {
		// Method to select from Drop down
		elementSelection = new Select(selectWebElement);

		if (option.equalsIgnoreCase("logout")) {
			elementSelection.selectByIndex(1);
		} else if (option.equalsIgnoreCase("account")) {
			elementSelection.selectByIndex(0);
		} else {
			System.out.println("Can't locate elements from top header list!");
		}
	}

}
